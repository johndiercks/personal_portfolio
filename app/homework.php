<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>John Diercks | Web Developer</title>
    <meta name="description" content="Portfolio website using the bulma framework">
    <meta name="author" content="John Diercks">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../node_modules/normalize.css/normalize.css">
    <link rel="stylesheet" href="./assets/styles/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto+Slab" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> 
    <script src="./assets/scripts/Vendor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@14/dist/smooth-scroll.polyfills.min.js"></script>

</head>
<body>

    <!-- start of navigation top right -->
    <nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation" id="navbar">
        <a id="nav-toggle" role="button" class="navbar-burger navbar-burger-custom" data-target="navMenu" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>

        <div class="navbar-menu navbar_custom navbar-end" id="navMenu">
            <!-- navbar-start, navbar-end... -->
            <a href="#about" class="navbar-item">About</a>
            <a href="#portfolio" class="navbar-item">Portfolio</a>
            <a href="#contact" class="navbar-item">Contact</a>
            <a href="index.html" class="navbar-item">Home</a>
        </div>
    </nav>

        <!-- beginning of start page and hero image section -->
        <section class="hero is-medium bg_custom">

        <div class="hero-body hero_custom">
        <div class="container has-text-centered hero_custom_container">
            <h1 class="title is-1 hero_custom_container_title">Hello I am John Diercks</h1>
            <h2 class="subtitle is-3 hero_custom_container_subtitle">Web Developer</h2>
        </div>
        </div>
    </section>
    <!-- end of start page and hero image section -->

    <!-- About section -->
    <section class="section about_section" id="about">
        <div class="container about">
        <h1 class="title has-text-centered about_text-title">Homework</h1>

        <h3 class="has-text-centered about_text-description">
            Welcome to my vast collection of homework an things that keep me busy when I'm bored.
            Have a look around and let me know what you think.
        </h3>
    
</body>
</html>