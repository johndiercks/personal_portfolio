import Navigation from './modules/Navigation';
import RevealOnScroll from './modules/RevealOnScroll';
// import BulmaIncludeNavigation from './modules/BulmaIncludeNavigation';
import $ from 'jquery';

new RevealOnScroll($(".about_column-container"), "25%");
new RevealOnScroll($(".projects-section"), "55%");