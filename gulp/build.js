var gulp = require('gulp');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');
var minifyCSS = require('gulp-minify-css');

gulp.task('image', function(){
    return gulp.src('./app/assets/images/**/*')
    .pipe(imagemin({
        progressive: true,
        interlaced: true,
        multipass: true
    }))
    .pipe(gulp.dest('./dist/images/'))
})

gulp.task('css', function(){
    return gulp.src('app/assets/styles/**/*.css')
    .pipe(minifyCSS({keepSpecialComments: 1}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('script', function () {
        gulp.src('./app/assets/scripts/**/*.js')
        .pipe(babel({presets: ["env"]}))
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/scripts'))
  });